FROM keymetrics/pm2:8-alpine
LABEL maintainer "Ryan Holt <draconusdesigns@gmail.com>"

# Install npm stuff.
RUN mkdir /api
WORKDIR /api
ADD package.json .
RUN npm install

# Bundle APP files
WORKDIR /api
ADD pm2.json .
ADD bin bin/
ADD server server/
ADD .sequelizerc .

# Show current folder structure in logs
RUN ls -alh /api
