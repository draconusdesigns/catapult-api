const bodyParser = require('body-parser');
const Bluebird = require('bluebird');
const config = require(`${__dirname}/config/config.js`);
const cors = require('cors');
const express = require('express');
const helmet = require('helmet');
const lodash = require('lodash');
const logger = require(`${__dirname}/config/logger.js`);
const morgan = require('morgan');
const nodeMoment = require('node-moment');
const useragent = require('express-useragent');
const app = express();

Promise = Bluebird;
_ = lodash;
moment = nodeMoment;

app.use(helmet());
app.use(cors());
app.options('*', cors());
app.use(require('express-favicon-short-circuit'));

app.use(useragent.express());

app.use(morgan('dev', {
    skip: (req, res) => res.statusCode < 400,
    stream: process.stderr
}));

app.use(morgan('dev', {
    skip: (req, res) => res.statusCode >= 400,
    stream: process.stdout
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/assets', express.static(`./${config.uploads}`));
require(`${__dirname}/config/swagger.js`)(app, config);
require(`${__dirname}/config/throttle.js`)(app, config);
require(`${__dirname}/config/session.js`)(app, config);
require('./routes')(app);
require(`${__dirname}/config/error.js`)(app, config, logger);

module.exports = app;
