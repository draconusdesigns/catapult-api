const jwtDecode = require('jwt-decode');
/**
* Deny
*
* Decodes user token, throwing a 403 error if user type
* is within restrictions array.
*
* @param {restrictions} array
* Valid Account Types: admin, customer
*
* __admin
* __customer
*
* @return {res}
*/
exports.deny = (restrictions = []) => {
    return async(req, res, next) => {
        try {
            let { accountType } = await jwtDecode(req.headers.authorization);

            if (restrictions.indexOf(accountType) >= 0) {
                throw({status: 403, message: "Forbidden"});
            }

            next();
        } catch (error) {
            next(error);
        }
    };
};
/**
* Allow
*
* Decodes user token, throwing a 403 error if user type
* is not within restrictions array.
*
* @param {restrictions} array
* Valid Account Types: admin, customer
*
* __admin
* __customer
*
* @return {res}
*/
exports.allow = (restrictions = []) => {
    return async(req, res, next) => {
        try {
            let { accountType } = await jwtDecode(req.headers.authorization);

            if (restrictions.indexOf(accountType) < 0) {
                throw({status: 403, message: "Forbidden"});
            }

            next();
        } catch (error) {
            next(error);
        }
    };
};
/**
* Owner
*
* Decodes user token, throwing a 403 error if user is not the owner
*
* @param {owns} object or null - Request key to check:
* Pass an object with params, body, or query key.
* Examples: {params: "id"} {body: "id"} {query: "id"}
*
* @return {res}
*/
exports.owner = (owns = {}) => {
    return async(req, res, next) => {
        try {
            let { id, accountType } = await jwtDecode(req.headers.authorization);
            let ownerId = await getOwnerId(req, owns);

            if (id != ownerId) {
                throw({status: 403, message: "Forbidden"});
            }

            next();
        } catch (error) {
            next(error);
        }
    };
};
/**
* Owner/Admin
*
* Decodes user token, throwing a 403 error if user is not either the owner
* of the record or an admin.
*
* @param {owns} object or null - Request key to check:
* Pass an object with params, body, or query key.
* Examples: {params: "id"} {body: "id"} {query: "id"}
*
* @return {res}
*/
exports.ownerAdmin = (owns = {}) => {
    return async(req, res, next) => {
        try {
            let { id, accountType } = await jwtDecode(req.headers.authorization);
            let ownerId = await getOwnerId(req, owns);

            if (id != ownerId && accountType != "admin") {
                throw({status: 403, message: "Forbidden"});
            }

            next();
        } catch (error) {
            next(error);
        }
    };
};
/**
* Get OwnerId from Request
*
* When given an owns object from the route, as well as the request,
* returns ownerId
*
* @param {req} object Request object
* @param {owns} object
*
* @return {ownerId} integer
*/
let getOwnerId = async(req, owns) => {
    if (owns.query) ownerId = req.query[owns.query];
    if (owns.body) ownerId = req.body[owns.body];
    if (owns.params) ownerId = req.params[owns.params];

    return ownerId;
}
