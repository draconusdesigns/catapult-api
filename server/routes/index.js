const jwt = require('jwt-express');
const bodyParser = require('body-parser');
const controllers = require('../controllers');
const guard = require('../middleware/route-guard.js');

module.exports = (app) => {
    /* Auth */
    app.post('/auth/register', controllers.auth.register);
    app.post('/auth/login', controllers.auth.login);
    /* Accounts */
    app.get('/accounts', [jwt.active(), guard.allow("admin")], controllers.accounts.list);
    app.get('/accounts/count', [jwt.active(), guard.allow("admin")], controllers.accounts.count);
    app.get('/accounts/:id', [jwt.active(), guard.ownerAdmin({params: "id"})], controllers.accounts.get);
    app.put('/accounts/:id', [jwt.active(), guard.ownerAdmin({params: "id"})], controllers.accounts.update);

    app.put('/accounts/:id/email', [jwt.active(), guard.ownerAdmin({params: "id"})], controllers.accounts.updateEmail);
    app.put('/accounts/:id/username', [jwt.active(), guard.ownerAdmin({params: "id"})], controllers.accounts.updateUsername);
    app.put('/accounts/:id/password', [jwt.active(), guard.ownerAdmin({params: "id"})], controllers.accounts.updatePassword);
    /* Validations */
    app.get('/validate/email/:email', jwt.active(), controllers.validations.email);
    app.post('/validate/password', jwt.active(), controllers.validations.password);
    app.get('/validate/username/:username', jwt.active(), controllers.validations.username);
    /* Catch All Route */
    app.get('*', (req, res) => {
        res.status(404).send({
            message: 'Space, the final frontier...'
        });
    });
};
