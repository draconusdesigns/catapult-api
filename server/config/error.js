module.exports = (app, config, logger) => {
    app.use((err, req, res, next) => {
        //Properly errors into clean error output
        if (err.error) {
            err.status = err.error.status;
            err.log = err.error.message;
            err.message = 'No record for resource found';
        }

        //Provide Different Info for each Logging Level
        logger.error(`${req.method} ${req.url} - ${err.log || err.message}`);
        logger.debug(JSON.stringify({referer: req.headers.referer, body: req.body}));
        logger.verbose(JSON.stringify({token: req.jwt.token}));
        logger.silly(JSON.stringify({error: err}));

        if (err.name == 'JWTExpressError') {
            err.status = 401;
        }

        switch (err.message) {
            case "JWT is stale":
                err.message = "Your session has expired, please log in again to continue";
                break;
            case "JWT is invalid":
                err.message = "Your session is invalid, please log in again to continue";
                break;
        }

        res.status(err.status || 400).send({
            message: `${err.message}`
        });
    });
};
