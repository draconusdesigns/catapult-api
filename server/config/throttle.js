module.exports = (app, config) => {
    if(config.requestThrottling && config.requestThrottling == 'true') {
        if (config.redis.enabled && config.redis.enabled == 'true') {
            const redis = require('redis').createClient({host: config.redis.url});
            const limiter = require('express-limiter')(app, redis);

            limiter({
                path: '*',
                method: 'all',
                lookup: ['connection.remoteAddress'],
                total: config.throttleLimit,
                expire: 1000
            });
        } else {
            const DDDoS = require('dddos');

            app.use(new DDDoS({
                rules: [{
                    regexp: ".*",
                    maxWeight: config.throttleLimit
                }],
                logFunction: (ip, path, currentWeight, maxWeight) => {
                    log.warn(`${ip} throttled from accessing ${path} - ${currentWeight}/${maxWeight} requests`)
                }
            }).express());
        }
    }
};
