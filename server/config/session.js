const jwt = require('jwt-express');
const config = require(`./config.js`);

module.exports = (app, config) => {
    app.use(jwt.init(config.session.tokenSecret, {
        cookies: false,
        stales: config.session.timeout * 60000
    }));
};
