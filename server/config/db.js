const dotenv = require('dotenv').config();

module.exports = {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: process.env.DB_DIALECT,
    operatorsAliases: false,
    logging: (process.env.DB_LOGGING && process.env.DB_LOGGING == 'true')? console.log : false,
    ssl: (process.env.DB_SSL && process.env.DB_SSL == 'true')? true : false
}
