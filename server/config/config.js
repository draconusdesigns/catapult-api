const dotenv = require('dotenv').config();

module.exports = {
    logLevel: process.env.LOG_LEVEL || 'debug',
    api: process.env.API,
    frontends: {
        web: process.env.WEB
    },
    redis: {
        enabled: (process.env.REDIS_ENABLED)? process.env.REDIS_ENABLED : false,
        url: process.env.REDIS_URL
    },
    requestThrottling: (process.env.REQUEST_THROTTLING)? process.env.REQUEST_THROTTLING : false,
    throttleLimit: (process.env.THROTTLE_LIMIT)? process.env.THROTTLE_LIMIT : 50,
    session: {
        timeout: (process.env.SESSION_TIMEOUT)? process.env.SESSION_TIMEOUT : 60,
        tokenSecret: process.env.TOKEN_SECRET
    }
}
