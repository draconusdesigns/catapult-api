const swaggerUi = require('swagger-ui-express');

module.exports = (app, config) => {
    let options = {
        swaggerOptions: {
            docExpansion: 'none',
            filter: true
        }
    };
    let swaggerDocument = {
        "openapi": "3.0.0",
        "servers": [
            {
                "url": config.api
            }
        ],
        "info": {
            "description": "An API that abstracts all levels of control.",
            "version": "1.0.0",
            "title": "Catapult Code Test API",
            "contact": {
                "email": "draconusdesigns@gmail.com"
            }
        },
        "tags": [
            {
                "name": "Accounts",
                "description": "Account routes"
            },
            {
                "name": "Auth",
                "description": "Authentication routes"
            },
            {
                "name": "Validations",
                "description": "Validation specific routes"
            }
        ],
        "paths": {
            "/auth/login": {
                "post": {
                    "tags": [
                        "Auth"
                    ],
                    "summary": "Login",
                    "description": "Allow users to log in, and receive a Token",
                    "requestBody": {
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/Login"
                                }
                            }
                        },
                        "description": "The username/password",
                        "required": true
                    },
                    "responses": {
                        "200": {
                            "description": "Login Success",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Token"
                                    }
                                }
                            }
                        },
                        "302": {
                            "description": "Login Redirect is returned if the user has logged into the wrong portal. This is an attempt by the API to provide a redirect to the appropriate portal. The token redirect url also contains an auth token as a query string, which will authenticate automatically to the portal being redirected to.",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/TokenRedirect"
                                    }
                                }
                            }
                        },
                        "401": {
                            "description": "If user is not found (bad credentials) OR if user can not login",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Error"
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "/accounts": {
                "get": {
                    "tags": [
                        "Accounts"
                    ],
                    "summary": "Get all accounts",
                    "description": "Get a list of all accounts.\n\n**Restrictions:**\n\nAllow accountType - admin",
                    "parameters": [
                        {
                            "name": "sortReverse",
                            "in": "query",
                            "description": "Boolean reverse sort",
                            "schema": {
                                "type": "boolean"
                            }
                        },
                        {
                            "name": "sortType",
                            "in": "query",
                            "description": "The attribute to sort by",
                            "schema": {
                                "type": "string",
                                "enum": [
                                    "id",
                                    "email",
                                    "username"
                                ]
                            }
                        },
                        {
                            "name": "limit",
                            "in": "query",
                            "description": "Max count of records to return",
                            "schema": {
                                "type": "integer"
                            }
                        },
                        {
                            "name": "offset",
                            "in": "query",
                            "description": "Offset the resultset of records to return",
                            "schema": {
                                "type": "integer"
                            }
                        }
                    ],
                    "responses": {
                        "200": {
                            "description": "An array of accounts",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "type": "array",
                                        "items": {
                                            "$ref": "#/components/schemas/Account"
                                        }
                                    }
                                }
                            }
                        },
                        "400": {
                            "description": "If an unexpected error is encountered",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Error"
                                    }
                                }
                            }
                        },
                        "403": {
                            "description": "If the route is forbidden for the current user",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Error"
                                    }
                                }
                            }
                        }
                    },
                    "security": [
                        {
                            "APIKey": []
                        }
                    ]
                }
            },
            "/accounts/count": {
                "get": {
                    "tags": [
                        "Accounts"
                    ],
                    "summary": "Count accounts",
                    "description": "Get a count of all existing accounts\n\n**Restrictions:**\n\nAllow accountType - admin",
                    "responses": {
                        "200": {
                            "description": "A count of accounts",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Count"
                                    }
                                }
                            }
                        },
                        "400": {
                            "description": "If an unexpected error is encountered",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Error"
                                    }
                                }
                            }
                        },
                        "403": {
                            "description": "If the route is forbidden for the current user",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Error"
                                    }
                                }
                            }
                        }
                    },
                    "security": [
                        {
                            "APIKey": []
                        }
                    ]
                }
            },
            "/accounts/{id}": {
                "get": {
                    "tags": [
                        "Accounts"
                    ],
                    "summary": "Find account by ID",
                    "description": "Returns a single Account\n\n**Restrictions:**\n\nAllow accountType - admin\n\nAllow Owner",
                    "parameters": [
                        {
                            "name": "id",
                            "in": "path",
                            "description": "ID of account to return",
                            "required": true,
                            "schema": {
                                "type": "integer"
                            }
                        }
                    ],
                    "responses": {
                        "200": {
                            "description": "A single account",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Account"
                                    }
                                }
                            }
                        },
                        "400": {
                            "description": "If an unexpected error is encountered",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Error"
                                    }
                                }
                            }
                        },
                        "403": {
                            "description": "If the route is forbidden for the current user",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Error"
                                    }
                                }
                            }
                        }
                    },
                    "security": [
                        {
                            "APIKey": []
                        }
                    ]
                },
                "put": {
                    "tags": [
                        "Accounts"
                    ],
                    "summary": "Update an existing account",
                    "description": "Updates an account\n\n**Restrictions:**\n\nAllow accountType - admin\n\nAllow Owner",
                    "parameters": [
                        {
                            "name": "id",
                            "in": "path",
                            "description": "ID of account to update",
                            "required": true,
                            "schema": {
                                "type": "integer"
                            }
                        }
                    ],
                    "requestBody": {
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/AccountEdit"
                                }
                            }
                        },
                        "description": "Account object that needs to be updated",
                        "required": true
                    },
                    "responses": {
                        "200": {
                            "description": "An update confirmation message",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Success"
                                    }
                                }
                            }
                        },
                        "400": {
                            "description": "If an unexpected error is encountered",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Error"
                                    }
                                }
                            }
                        },
                        "403": {
                            "description": "If the route is forbidden for the current user",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Error"
                                    }
                                }
                            }
                        }
                    },
                    "security": [
                        {
                            "APIKey": []
                        }
                    ]
                }
            },
            "/accounts/{id}/email": {
                "put": {
                    "tags": [
                        "Accounts"
                    ],
                    "summary": "Update the Email Address for an Account",
                    "description": "Update the Email Address associated with an Account\n\n**Restrictions:**\n\nAllow accountType - admin\n\nAllow Owner",
                    "parameters": [
                        {
                            "name": "id",
                            "in": "path",
                            "description": "ID of Account to update the Email Address for",
                            "required": true,
                            "schema": {
                                "type": "integer"
                            }
                        }
                    ],
                    "requestBody": {
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "email_address": {
                                            "type": "string"
                                        }
                                    }
                                }
                            }
                        },
                        "description": "Email Address object that needs to be updated",
                        "required": true
                    },
                    "responses": {
                        "200": {
                            "description": "A message confirming successful email address update",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Success"
                                    }
                                }
                            }
                        },
                        "400": {
                            "description": "If an unexpected error is encountered",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Error"
                                    }
                                }
                            }
                        },
                        "403": {
                            "description": "If the route is forbidden for the current user",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Error"
                                    }
                                }
                            }
                        }
                    },
                    "security": [
                        {
                            "APIKey": []
                        }
                    ]
                },
            },
            "/accounts/{id}/username": {
                "put": {
                    "tags": [
                        "Accounts"
                    ],
                    "summary": "Update the Username for an Account",
                    "description": "Update the Username associated with an Account\n\n**Restrictions:**\n\nAllow accountType - admin\n\nAllow Owner",
                    "parameters": [
                        {
                            "name": "id",
                            "in": "path",
                            "description": "ID of Account to update the Username for",
                            "required": true,
                            "schema": {
                                "type": "integer"
                            }
                        }
                    ],
                    "requestBody": {
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "username": {
                                            "type": "string"
                                        }
                                    }
                                }
                            }
                        },
                        "description": "Username object that needs to be updated",
                        "required": true
                    },
                    "responses": {
                        "401": {
                            "description": "A unique object containing the new token",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/UsernameUpdate"
                                    }
                                }
                            }
                        },
                        "400": {
                            "description": "If an unexpected error is encountered",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Error"
                                    }
                                }
                            }
                        },
                        "403": {
                            "description": "If the route is forbidden for the current user",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Error"
                                    }
                                }
                            }
                        }
                    },
                    "security": [
                        {
                            "APIKey": []
                        }
                    ]
                },
            },
            "/accounts/{id}/password": {
                "put": {
                    "tags": [
                        "Accounts"
                    ],
                    "summary": "Update the Password for an Account",
                    "description": "Update the Password associated with an Account\n\n**Restrictions:**\n\nAllow accountType - admin\n\nAllow Owner",
                    "parameters": [
                        {
                            "name": "id",
                            "in": "path",
                            "description": "ID of Account to update the Password for",
                            "required": true,
                            "schema": {
                                "type": "integer"
                            }
                        }
                    ],
                    "requestBody": {
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "password": {
                                            "type": "string"
                                        }
                                    }
                                }
                            }
                        },
                        "description": "Password object that needs to be updated",
                        "required": true
                    },
                    "responses": {
                        "200": {
                            "description": "A message confirming successful password update",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Success"
                                    }
                                }
                            }
                        },
                        "400": {
                            "description": "If an unexpected error is encountered",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Error"
                                    }
                                }
                            }
                        },
                        "403": {
                            "description": "If the route is forbidden for the current user",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Error"
                                    }
                                }
                            }
                        }
                    },
                    "security": [
                        {
                            "APIKey": []
                        }
                    ]
                },
            },
            "/validate/email/{email}": {
                "get": {
                    "tags": [
                        "Accounts",
                        "Validations"
                    ],
                    "summary": "Validate Email is Unique",
                    "description": "Returns a Unique Object",
                    "parameters": [
                        {
                            "name": "email",
                            "in": "path",
                            "description": "Email to check uniqueness",
                            "required": true,
                            "schema": {
                                "type": "string"
                            }
                        }
                    ],
                    "responses": {
                        "200": {
                            "description": "A Unique object",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Unique"
                                    }
                                }
                            }
                        },
                        "400": {
                            "description": "If an unexpected error is encountered",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Error"
                                    }
                                }
                            }
                        }
                    },
                    "security": [
                        {
                            "APIKey": []
                        }
                    ]
                },
            },
            "/validate/password": {
                "post": {
                    "tags": [
                        "Accounts",
                        "Validations"
                    ],
                    "summary": "Validate Password is Matching",
                    "description": "Returns a Boolean value",
                    "requestBody": {
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "object",
                                    "properties": {
                                        "password": {
                                            "type": "string"
                                        }
                                    }
                                }
                            }
                        },
                        "description": "**Required:**",
                        "required": true
                    },
                    "responses": {
                        "200": {
                            "description": "A Boolean value"
                        },
                        "400": {
                            "description": "If an unexpected error is encountered",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Error"
                                    }
                                }
                            }
                        }
                    },
                    "security": [
                        {
                            "APIKey": []
                        }
                    ]
                },
            },
            "/validate/username/{username}": {
                "get": {
                    "tags": [
                        "Accounts",
                        "Validations"
                    ],
                    "summary": "Validate Username is Unique",
                    "description": "Returns a Unique Object",
                    "parameters": [
                        {
                            "name": "username",
                            "in": "path",
                            "description": "Username to check uniqueness for",
                            "required": true,
                            "schema": {
                                "type": "string"
                            }
                        }
                    ],
                    "responses": {
                        "200": {
                            "description": "A Unique object",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Unique"
                                    }
                                }
                            }
                        },
                        "400": {
                            "description": "If an unexpected error is encountered",
                            "content": {
                                "application/json": {
                                    "schema": {
                                        "$ref": "#/components/schemas/Error"
                                    }
                                }
                            }
                        }
                    },
                    "security": [
                        {
                            "APIKey": []
                        }
                    ]
                },
            }
        },
        "components": {
            "securitySchemes": {
                "APIKey": {
                    "description": "For accessing most API routes, a valid JWT token must be present.\n\nA valid JWT token is generated by the API and returned as a response to a call\nto the route /auth/login, passing a valid user & password with the request.",
                    "type": "http",
                    "scheme": "bearer",
                    "bearerFormat": "TOKEN"
                }
            },
            "schemas": {
                "Account": {
                    "type": "object",
                    "properties": {
                        "data": {
                            "type": "array",
                            "items": {
                                "type": "object",
                                "properties": {
                                    "id": {
                                        "type": "integer"
                                    },
                                    "email": {
                                        "type": "string"
                                    },
                                    "username": {
                                        "type": "string"
                                    }
                                }
                            }
                        },
                        "meta": {
                            "type": "object",
                            "properties": {
                                "count": {
                                    "type": "integer"
                                },
                                "limit": {
                                    "type": "integer"
                                },
                                "offset": {
                                    "type": "integer"
                                },
                                "sortType": {
                                    "type": "string"
                                },
                                "sortReverse": {
                                    "type": "boolean"
                                }
                            }
                        }
                    }
                },
                "AccountEdit": {
                    "type": "object",
                    "properties": {
                        "email": {
                            "type": "string"
                        },
                        "password": {
                            "type": "string"
                        }
                    },
                    "required": [
                        "email",
                        "password"
                    ]
                },
                "Count": {
                    "type": "object",
                    "properties": {
                        "count": {
                            "type": "integer"
                        }
                    }
                },
                "Error": {
                    "type": "object",
                    "properties": {
                        "message": {
                            "type": "string"
                        }
                    }
                },
                "Login": {
                    "type": "object",
                    "properties": {
                        "username": {
                            "type": "string"
                        },
                        "password": {
                            "type": "string"
                        }
                    }
                },
                "Success": {
                    "type": "object",
                    "properties": {
                        "message": {
                            "type": "string"
                        }
                    }
                },
                "Token": {
                    "type": "object",
                    "properties": {
                        "token": {
                            "type": "string"
                        },
                        "payload": {
                            "type": "object",
                            "properties": {
                                "id": {
                                    "type": "integer"
                                },
                                "username": {
                                    "type": "string"
                                },
                                "accountType": {
                                    "type": "string"
                                },
                                "role": {
                                    "type": "string"
                                },
                                "timezoneOffset": {
                                    "type": "integer"
                                },
                                "stales": {
                                    "type": "integer"
                                },
                                "iat": {
                                    "type": "integer"
                                }
                            }
                        },
                        "valid": {
                            "type": "boolean"
                        },
                        "expired": {
                            "type": "boolean"
                        },
                        "stale": {
                            "type": "boolean"
                        }
                    }
                },
                "TokenRedirect": {
                    "type": "object",
                    "properties": {
                        "location": {
                            "type": "string"
                        }
                    }
                },
                "Unique": {
                    "type": "object",
                    "properties": {
                        "unique": {
                            "type": "boolean"
                        }
                    }
                },
                "UsernameUpdate": {
                    "type": "object",
                    "properties": {
                        "token": {
                            "type": "string"
                        }
                    }
                }
            }
        }
    };

    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, options));
}
