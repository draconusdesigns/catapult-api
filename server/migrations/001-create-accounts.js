module.exports = {
    up: function(queryInterface, Sequelize) {
        queryInterface.sequelize.query('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')
            .then(() => {
                queryInterface.createTable('Accounts', {
                    id: {
                        allowNull: false,
                        primaryKey: true,
                        type: Sequelize.DataTypes.UUID,
                        defaultValue: Sequelize.literal('uuid_generate_v4()')
                    },
                    type: {
                        type: Sequelize.INTEGER,
                        allowNull: false,
                        defaultValue: 2
                    },
                    username: {
                        type: Sequelize.STRING,
                        allowNull: false,
                        unique: true
                    },
                    firstname: {
                        type: Sequelize.STRING,
                        allowNull: false,
                        unique: true
                    },
                    lastname: {
                        type: Sequelize.STRING,
                        allowNull: false,
                        unique: true
                    },
                    email: {
                        type: Sequelize.STRING,
                        allowNull: false,
                        unique: true
                    },
                    password: {
                        type: Sequelize.STRING,
                        allowNull: false
                    },
                    createdAt: {
                        type: Sequelize.DATE,
                        allowNull: false
                    },
                    updatedAt: {
                        type: Sequelize.DATE,
                        allowNull: false
                    },
                    deletedAt: {
                        type: Sequelize.DATE,
                        allowNull: true
                    }
                });
            });
    },
    down: function(queryInterface) { queryInterface.dropTable('Accounts') }
};
