const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(module.filename);
const config = require(`${__dirname}/../config/config.js`);
const dbConfig = require(`${__dirname}/../config/db.js`);

const db = {};

let sequelize;

if (config.use_env_variable) {
    sequelize = new Sequelize(process.env[config.use_env_variable]);
} else {
    sequelize = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, {
        host: dbConfig.host,
        port: dbConfig.port,
        dialect: dbConfig.dialect,
        ssl: dbConfig.ssl,
        dialectOptions: {
            ssl: dbConfig.ssl
        },
        operatorsAliases: dbConfig.operatorsAliases,
        pool: {
            max: 10,
            min: 0,
            idle: 30000,
            acquire: 60000
        },
        logging: dbConfig.logging
    });
}

fs
    .readdirSync(__dirname)
    .filter((file) =>
        (file.indexOf('.') !== 0) &&
        (file !== basename) &&
        (file.slice(-3) === '.js'))
    .forEach((file) => {
        const model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach((modelName) => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
