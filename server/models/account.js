const Transformer = require('object-transformer');
const config = require(`${__dirname}/../config/config.js`);
const CryptographyHelper = require('../helpers/cryptography');

module.exports = (sequelize, DataTypes) => {
    const Account = sequelize.define('Account', {
        type: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 2
        },
        username: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        firstname: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        lastname: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        paranoid: true,
        hooks: {
            beforeCreate: (account) => {
                return new Promise((resolve, reject) => {
                    let hash = CryptographyHelper.generatePasswordHash(account.password);
                    account.password = hash;

                    resolve(account);
                });
            }
        }
    });

    const schema = {
        id: "id",
        username: "username",
        firstname: "firstname",
        lastname: "lastname",
        email: "email"
    };

    Account.list = (models) => new Transformer.List(models, schema).parse();

    Account.single = (model) => new Transformer.Single(model, schema).parse();

    Account.prototype.validPassword = function(password) {
        return new Promise((resolve, reject) => {
            let hash = CryptographyHelper.generatePasswordHash(password);
            let valid = (hash === this.password)? true : false;

            resolve(valid);
        });
    };
    /**
     * Get the accountType for the current account
     *
     * @param {account} obj
     *
     * @return {accountType} string
     */
    Account.prototype.getAccountType = (account) => {
        let accountType = false;

        switch(account.type) {
            case 1:
                accountType = 'admin';
                break;
            case 2:
                accountType = 'customer';
                break;
        }

        return accountType;
    };

    return Account;
};
