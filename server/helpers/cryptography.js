const config = require(`${__dirname}/../config/config.js`);
const crypto = require('crypto');
/**
* Generate Password Hash
*
* 1000 Pass Sha512 64 character length password hash
*
* @param {password} string
*
* @return {hash}
*/
exports.generatePasswordHash = (password) => {
    return crypto.pbkdf2Sync(password, config.session.tokenSecret, 1000, 64, `sha512`).toString(`hex`);
};
/**
* Generate Hexidecimal String
*
* Generates a Hexidecimal String of the passed in length;
*
* @param {length} int default 8
*
* @return {code}
*/
exports.generateHexString = (length = 8) => {
    if (length <= 0) return '';
    let code;

    try {
        code = crypto.randomBytes(Math.ceil(length / 2)).toString('hex').slice(0, length);
        /* note: could do this non-blocking, but still might fail */
    } catch(error) {
        /* known exception cause: depletion of entropy info for randomBytes */
        console.error('Exception generating random string: ' + error);
        /* weaker random fallback */
        code = '';
        let r = length % 8, q = (length - r)/8, i;

        for(i = 0; i < q; i++) {
            code += Math.random().toString(16).slice(2);
        }

        if(r > 0) code += Math.random().toString(16).slice(2, i);
    }

    return code.toUpperCase();
};
