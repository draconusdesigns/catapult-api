/**
* Search By Text
*
* Takes JSON in and Filters the entire collection by the provided text.
*
* @param {collection} collection
* @param {text} string text to search by
* @param {exclude} string text to exclude by
*
* @return {collection}
*/
exports.searchByText = (collection, text, exclude) => {
    text = _.toLower(text);

    return _.filter(collection, (object) => {
        return _(object).omit(exclude).some((string) => {
            return _(string).toLower().includes(text);
        });
    });
};
