const Account = require('../models').Account;
const CryptographyHelper = require('../helpers/cryptography');

module.exports = (config, jwtDecode, Op, logger) => {
    let validations = {};
    /**
    * Validate unique email
    *
    * @param {email} string Email
    *
    * @return {unique} res
    */
    validations.email = async(req, res, next) => {
        try {
            let unique = await Account.count({
                where: {
                    email: req.params.email
                }
            });

            logger.info(`Email was found to be unique: ${!unique}`);
            res.status(200).send({unique: !unique});
        } catch (error) {
            next(error);
        }
    };
    /**
    * Validate password
    *
    * @return {valid} res
    */
    validations.password = async(req, res, next) => {
        try {
            let user = jwtDecode(req.headers.authorization);
            let account = await Account.findOne({where: {id: user.id}});
            if (!account) throw({status: 401, message: 'Username/ Password Wrong'});

            let hash = CryptographyHelper.generatePasswordHash(req.body.password);
            let valid = (hash === account.password)? true : false;

            logger.info(`New Password is valid: ${valid}`);
            res.status(200).send({valid});
        } catch (error) {
            next(error);
        }
    };
    /**
    * Validate unique username
    *
    * @param {username} string Username
    *
    * @return {unique} res
    */
    validations.username = async(req, res, next) => {
        try {
            let unique = await Account.count({
                where: {
                    username: req.params.username
                }
            });

            logger.info(`Username was found to be unique: ${!unique}`);
            res.status(200).send({unique: !unique});
        } catch (error) {
            next(error);
        }
    };

    return validations;
}
