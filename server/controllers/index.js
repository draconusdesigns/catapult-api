const config = require(`${__dirname}/../config/config.js`);
const logger = require(`${__dirname}/../config/logger.js`);
const jwtDecode = require('jwt-decode');
const Op = require('sequelize').Op;

const accounts = require('./accounts');
const auth = require('./auth');
const validations = require('./validations');

module.exports = {
    accounts: accounts(config, jwtDecode, Op, logger),
    auth: auth(config, jwtDecode, Op, logger),
    validations: validations(config, jwtDecode, Op, logger)
};
