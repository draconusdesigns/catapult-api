const Account = require('../models').Account;
const CryptographyHelper = require('../helpers/cryptography');

module.exports = (config, jwtDecode, Op, logger) => {
    let accounts = {};
    /**
     * Get count of Accounts;
     *
     * @return {Object} Count object of how many accounts there are
     */
    accounts.count = async(req, res, next) => {
        try {
            let count = await Account.count();
            res.status(200).send({count:count});
        } catch (error) {
            next(error);
        }
    };
    /**
    * Get a list of all Accounts
    *
    * Returns a list of all accounts except the
    * user requesting the data. Password and
    * other date useless to the user is ommited from the results.
    *
    * @return {accounts} collection
    */
    accounts.list = async(req, res, next) => {
        try {
            let order = [];

            if (req.query.sortType) {
                switch (req.query.sortType) {
                    case 'id':
                        order.push(['id', (req.query.sortReverse === "true")? 'DESC':'ASC']);
                        break;
                    case 'email':
                        order.push(['email', (req.query.sortReverse === "true")? 'DESC':'ASC']);
                        break;
                    case 'username':
                        order.push(['username', (req.query.sortReverse === "true")? 'DESC':'ASC']);
                        break;
                    default:
                        order.push([req.query.sortType, (req.query.sortReverse === "true")? 'DESC':'ASC']);
                        break;
                }
            };

            let accounts = await Account.findAndCountAll({
                order,
                limit: req.query.limit,
                offset: req.query.offset
            });

            res.status(200).send({
                data: await Account.list(accounts.rows),
                meta: {
                    count: accounts.count,
                    limit: req.query.limit,
                    offset: req.query.offset,
                    sortType: req.query.sortType,
                    sortReverse: req.query.sortReverse
                }
            });
        } catch (error) {
            next(error);
        }
    };
    /**
     * Get the Account;
     *
     * @return {Object} The account
     */
    accounts.get = async(req, res, next) => {
        try {
            let account = await Account.findOne({where: {id:req.params.id}});
            res.status(200).send(await Account.single(account));
        } catch (error) {
            next(error);
        }
    };
    /**
     * Update the Account
     *
     * @return {Object} The updated account
     */
    accounts.update = async(req, res, next) => {
        try {
            let user = jwtDecode(req.headers.authorization);
            let account = await Account.findOne({where: {id: user.id}});

            account = await account.update({
                username: req.body.username,
                email: req.body.email,
                phone: req.body.phone,
                password: req.body.password
            });

            res.status(200).send(await Account.single(account));
        } catch (error) {
            next(error);
        }
    };
    /**
    * Update the Email for an Account
    *
    * @param {id} int accountId
    *
    * @return {message} res
    */
    accounts.updateEmail = async(req, res, next) => {
        try {
            let account = await Account.update({
                email: req.body.email
            }, {
                where: {id: req.params.id}
            });

            logger.info(`Email Updated for Account ${req.params.id}`);
            res.status(200).send({message: "Email updated succesfully!"});
        } catch (error) {
            next(error);
        }
    }
    /**
    * Update the Username for an Account
    *
    * @param {id} int accountId
    *
    * @return {token} res
    */
    accounts.updateUsername = async(req, res, next) => {
        const jwt = require('jwt-express');

        try {
            let account = await Account.findOne({where: {id: req.params.id}});
            account.update({username: req.body.username});

            let token = jwt.create(process.env.TOKEN_SECRET, {
                id: account.id,
                username: account.username
            }).token;

            logger.info(`Username Updated for Account ${req.params.id}`);
            res.status(401).send({token});
        } catch (error) {
            next(error);
        }
    }
    /**
    * Update the Password for an Account
    *
    * @param {id} int accountId
    *
    * @return {message} res
    */
    accounts.updatePassword = async(req, res, next) => {
        const jwt = require('jwt-express');
        try {
            let account = await Account.findOne({
                where: {id: req.params.id}
            });

            let hash = CryptographyHelper.generatePasswordHash(req.body.password);
            account.update({password: hash});

            logger.info(`Password Updated for Account ${req.params.id}`);
            res.status(200).send({message: "Password updated succesfully!"});
        } catch (error) {
            next(error);
        }
    }

    return accounts;
};
