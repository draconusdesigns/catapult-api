const Account = require('../models').Account;
const CryptographyHelper = require('../helpers/cryptography');

module.exports = (config, jwtDecode, Op, logger) => {
    let auth = {};
    /**
    * Process the user registration, generating and returning a token if successful.
    *
    * @return {res}
    */
    auth.register = async(req, res, next) => {
        try {
            let account = await Account.create({
                type: 2,
                email: req.body.email,
                username: req.body.username,
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                password: req.body.password
            });

            account.accountType = await account.getAccountType(account);

            res.status(200).send(res.jwt({
                id: account.id,
                username: account.username,
                accountType: account.accountType,
                role: account.role
            }));
        } catch (error) {
            next(error);
        }
    };
    /**
    * Process the user login, generating and returning a token if successful.
    *
    * @return {res}
    */
    auth.login = async(req, res, next) => {
        try {
            let account = await Account.findOne({
                where: {
                    [Op.or]: [
                        { email: req.body.username },
                        { username: req.body.username }
                    ]
                }
            });

            let valid = await account.validPassword(req.body.password);

            if (!valid) {
                return next({status: 401, message: 'Username/Password Wrong'});
            }

            account.accountType = await account.getAccountType(account);

            res.status(200).send(res.jwt({
                id: account.id,
                username: account.username,
                accountType: account.accountType,
                role: account.role
            }));
        } catch (error) {
            next({status: 401, message: 'Username/Password Wrong'});
        }
    };

    return auth;
};
