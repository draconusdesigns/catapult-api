# API

API built using Nodejs, Express 4.16, and Sequelize ORM;

### List of features:

- MVC Architecture
- Sequelize ORM
- Database Migrations and Seeders
- CLI Model and Migration Creation

## Setup and install

#### Dependency installation

During the time building this project, you'll need development dependencies of which run on Node.js, follow the steps below for setting everything up (if you have some of these already, skip to the next step where appropriate):

1. Download and install [Node.js here](https://nodejs.org/en/download/) for Windows or for Mac.
2. Install Sequelize CLI on the command-line Globally with `npm install -g sequelize-cli`

That's about it for tooling you'll need to run the project, let's move onto the project install.

#### Project installation and server

Now you've pulled down the repo and have everything setup, you'll need to `cd` into the directory that you cloned the repo into and run some quick tasks:

```
cd <api-folder>
npm i
```

This will then setup all the development and production dependencies we need.

Configure the API config settings by copying the `.env.example` on the root of the project to a file on the root of the project named `.env`. This is where all the application configurations will live.

#### Configure and Migrate the Database
The ORM of choice (Sequelize) for this project is much like a mix of Laravel Eloquent ORM as well as the excellent scaffolding provided with PHP Artisan. Sequelize can generate models, seeds, migrations, and more right from the commandline, saving much in the way of coding.

This similar to ORM functionality found in most major MVC frameworks, including Phoenix(for Elixir/Erlang), as well as Ruby on Rails as well, and enables us to rapidly develop fully functioning projects quickly.

The DB Seeding documentation seems lacking in the Sequelize docs, but this is only because seeds are basically identical to migrations but use a different folder and meta table, and also a slightly different set of cli commands. You can always check the list of available commands by running `sequelize help`

In terminal, run the following command to migrate the DB:
`sequelize db:migrate`

In terminal, run the following command to seed the DB:
`sequelize db:seed:all`

For more information on creating and running migrations, check out Sequelizes awesome docs:
[Sequelize Migration Reference](http://docs.sequelizejs.com/manual/tutorial/migrations.html)

Everything you do in the API project will be inside of `/server`.

#### Running the local server

```
npm start
```

#### Running in Docker - Recommended for Demo
If you don't have Docker installed on the dev machine already, do so now, you can get it here:
[Docker Desktop](https://www.docker.com/products/docker-desktop)

Once installed, we need to simply launch the containers, using the following command in the Docker terminal:

```
docker-compose up --build -d
```

After built and running, you can migrate the database from within your local env by changing the .env setting for DB_HOST=localhost, then in terminal, run the following command to migrate the DB:
`sequelize db:migrate`

In terminal, run the following command to seed the DB:
`sequelize db:seed:all`

However please keep in mind that Docker will want DB_HOST=postgres-db, but the .env is parsed in at container build time, so changing this after without rebuilding the docker container is fine.

To see the logs for the API while it is running is simple as well, in Docker terminal:
```
docker ps
```
This will provide you with an output similar to the following:
```
CONTAINER ID        IMAGE                COMMAND                  CREATED             STATUS              PORTS                                                NAMES
d5c331dc6c4f        api_api              "sh -c 'npm install …"   11 minutes ago      Up 11 minutes       80/tcp, 443/tcp, 43554/tcp, 0.0.0.0:8000->8000/tcp   api_api_1
9e580c29168a        postgres:10-alpine   "docker-entrypoint.s…"   11 minutes ago      Up 11 minutes       0.0.0.0:5432->5432/tcp                               api_postgres-db_1
f9b634e6e2f9        redis:alpine         "docker-entrypoint.s…"   11 minutes ago      Up 11 minutes       0.0.0.0:6379->6379/tcp                               api_redis-api_1
```

Copy the CONTAINER ID for api_api, in this case `d5c331dc6c4f`, and call docker logs on that container id with the -f follow flag enabled, like so:

```
docker logs -f d5c331dc6c4f
```

Now you can follow the API logs real-time
