// This will be our application entry. We'll setup our server here.
const app = require('../server/app.js');
const fs = require('fs');
const logger = require(`../server/config/logger.js`);
const port = parseInt(process.env.PORT, 10) || 8000;

switch (process.env.PROXY_SCHEMA) {
    case "https": // start https server
        const https = require('https');
        let sslOptions = {
           key: fs.readFileSync('/etc/ssl/key.pem'),
           cert: fs.readFileSync('/etc/ssl/cert.pem')
        };
        let serverHttps = https.createServer(sslOptions, app)
            .listen(port, () => logger.info(`The server is securely listening on port ${port}`));
        break;
    default: // start http server
        const http = require('http');
        let server = http.createServer(app)
            .listen(port, () => logger.info(`The server is listening on port ${port}`));
        break;
}
